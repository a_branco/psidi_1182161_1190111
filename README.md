# 0. API Reviews #

Os produtos são caracterizados por uma designação e descrição, bem como um conjunto de imagens do produto e um SKU (Stock Keeping Unit).
Os utilizadores podem classificar um produto fornecendo um texto e uma avaliação (de 0 a 5 estrelas, é também possível dar meias estrelas)
As reviews precisam de ser aprovadas por um moderador antes de serem públicas. Outros utilizadores podem verificar a review e um resumo do item incluindo a avaliação agregada de 5 estrelas, bem como review individuais. 
Os utilizadores também podem votar numa review se a considerar particularmente util, bem como reportar uma review se a considerar ofensiva ou enganosa. Comentários que foram reportados 
serão aprovados ou rejeiados por um moderador.
Além de avaliações e avaliações individuais, um produto tem uma avaliação agregada com base na média ponderada de todas as classificações. Os detalhes da classificação agregada estão na forma de uma tabela de frequência 
(a frequência de votos por estrela).

### 0.1. Use Cases ###

**Assignment #1**

- [DONE] 1. Como cliente quero registar-me no sistema.  
- [DONE] 2. Como cliente anónimo ou registado quero obter o catálogo de produtos. O retorno deve ser paginado.  
- [DONE] 3. Como cliente anónimo ou registado, quero obter os detalhes de um produto. Deve ser possível obter as imagens do produto e o SKU como uma imagem de código de barras.  
- [DONE] 4. Como cliente anónimo ou registado, quero obter as avaliações de um produto. Apenas reviews públicas devem ser obtidas. O retorno deve ser paginado. O retorno deve ser ordenado ao contrário pela data de publicação cronológica.  
- [DONE] 5. Como cliente registado, quero rever e avaliar um produto. Este é um pedido assíncrono.  
- [DONE] 6. Como cliente registado, quero verificar o estado de uma revisão que publiquei para que eu saiba que foi publicado ou rejeitado.  
- [DONE] 7. Como cliente registado, quero obter todas as minhas avaliações, incluindo o seu estado.  
- [DONE] 8. Como cliente registado, quero filtrar as minhas avaliações por estado  
- [DONE] 9. Como moderador, quero obter todas as avaliações pendentes  
- [DONE] 10. Como moderador, quero aprovar ou rejeitar uma revisão pendente.  
- [DONE] 11. Como desenvolvedor da API, quero exemplos dos pedidos para todos os casos de utilização (por exemplo, coleção POSTMAN) com testes.  
- [BONUS] 12. Aumentar a review com um facto engraçado sobre a data de publicação usando um serviço público, por exemplo, http://numbersapi.com/9/21/date.  
- [BONUS] 13. Como cliente anónimo ou registado, quero pesquisar no catálogo de produtos por nome de produto ou código de barras. O retorno deve ser paginado.  
- [BONUS] 14. Utilzar autenticação OAuth em todos pedidos do cliente.

**Assignment #2**

- [DONE] 15. Como cliente registado quero votar numa review.  
- [DONE] 16. Como cliente registado quero remover uma das minhas reviews. Só é possível se a avaliação não tiver votos.
- [] 17. Como cliente anónimo ou registado, desejo obter as reviews de um produto, paginadas, ordenadas por número de votos e data de publicação.  
- [] 18. Como cliente anónimo ou registado desejo obter a avaliação agregada de um produto.  
- [] 19. Como cliente registado quero reportar uma review.  
- [] 20. Como moderador quero obter todas as reviews reportadas.
- [] 21. Como moderador quero aprovar ou rejeitar uma review reportada. As reviews que forem aprovadas são novamente publicadas.  
- [] 22. Como desenvolvedor da API, quero que todas as representações retornem links hipermedia.
- [DONE] 23. Como desenvolvedor da API, quero uma API Billboard.
- [] 24. Como desenvolvedor da API, quero pedidos ezemplo para todos os casos de uso (por exemplo, coleção POSTMAN) com testes.

Nota:  
Uma vez que não é para nos concentrarmos nos aspetos de persistência ou lógica de negócio do sistema, mas apenas na camada de serviço, é aceitável que a "base de dados de produtos" seja uma loja de memórias. Se preferir ter uma API para adicionar moderadores e produtos devemos utilizar um caminho distinto da API principal, por exemplo, /admin.

### 0.2. Tarefas ###

**Assignment #1**

- [DONE] 1. Definir a arquitetura do sistema e fornecer a sua lógica (capítulo 2)  
- [DONE] 2. Definir o protocolo de interação (capítulo 3)  
- [DONE] 3. Justificar as decisões (capítulo 4)  
- [DONE] 4. Criar a especificação OpenAPI dos serviços(s) do sistema descrito (capítulo 6)
- [DONE] 5. Desenvolver o protótipo do sistema descrito para os casos de utilização listados  
- [DONE] 6. Autoavaliação e avaliação dos elementos do grupo (capítulo 7)  

**Assignment #2**  
  
- [DONE] 1. Desenvolver o diagrama DAP (Domain Application Protocol) para uma versão hipermedia do sistema, considerando os casos de uso antigos e novos (capítulo 5 do modelo de relatório);  
- [DONE] 2. Rever a arquitetura, protocolo de interação e decisões de design com hipermedia em mente (capítulos 2, 3 e 4 do modelo de relatório);  
- [DONE] 3. Adaptar os casos de uso existentes a recursos hipermedia;  
- [DONE] 4. Criar a especificação OpenAPI ou HAL do(s) serviço(s) do sistema descrito (capítulo 6 do modelo de relatório);  
- [DONE] 5. Estender o protótipo para os casos de uso listados, levando em consideração:  
	- O sistema deve ser um REST Nível 3 API;  
	- Tudo o que está em vigor no Assignment #1 ainda é válido;  
	- A comunicação com o departamento financeiro é assíncrona e deve ser confiável (???)
- [DONE] 6. Autoavaliação e avaliação dos pares (capítulo 7 do modelo de relatório).

Nota: A conceção do sistema (correspondente aos capítulos 2 a 4 do modelo de relatório) deve ser feito antes do desenvolvimento do sistema.

# 1. Architecture #

![architecture](https://bitbucket.org/a_branco/psidi_1182161_1190111/raw/b144a15653c72554f568f6f58c2610f77316475a/ReportTemplate/Architecture-1.jpg)


# 2. Interaction protocol #

  
# 3. How to run? #

- Users service (folder UsersService)  
	`npm install && npm start
	`

- Catalogue Service (folder CatalogueService)  
	`npm install && npm start
	`

- Reviews Service (folder ReviewsService)  
	`npm install && npm start
	`
	
Os serviços ficarão a correr nos seguintes endereços:  
Users Service: `http://localhost:8001/`  
Catalogue Service: `http://localhost:8003/`  
Reviews Service: `http://localhost:8002/` 

# 4. API Billboard #

# 5. OpenAPI Specification Files #

# 4. Postman Collection #