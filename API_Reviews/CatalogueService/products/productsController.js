///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// PRODUCTS CONTROLLER
///////////////////////////////////////////////////////
// For the generation of barcode images used the following site: https://barcode.tec-it.com

"use strict";

// NPM Dependencies Section
const express = require('express'),
    router = express.Router(),
    _ = require('underscore'),
    axios = require('axios'),
    mcache = require('memory-cache'),
    fresh = require('fresh');

// Imports Section
const productsService = require('./productsService'),
    errorHandler = require('../helpers/error-handler'),
    utils = require('../helpers/utils'),
    config = require('../config.json');

// Cache middleware
var cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url
        let cachedBody = mcache.get(key)
        if (cachedBody) {
            res.send(cachedBody)
            return
        } else {
            res.sendResponse = res.send
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body)
            }
            next()
        }
    }
}


// ROUTES
// GET
router.get('/', cache(86400), getAllProducts); // HTTP Cache 1 day
router.get('/:productSKU', getProduct);
router.get('/:productName', getProductByName);
router.get('/:productSKU/reviews', getReviewsByProduct);
router.get('/:productSKU/images', getImagesByPrduct);
router.get('/:productSKU/reviews/summary', getReviewsSummaryByProduct);

// POST
router.post('/', addNewProduct);

// FUNCTIONS

// Add new product
function addNewProduct(req, res, next) {
    const product = req.body;
    productsService.addProduct(product.data)
        .then(r => {
            errorHandler.send(200, r, req, res, 'application/json');
        })
        .catch(next)
}

// Returns barcode as image
function getImagesByPrduct(req, res, next) {
    const productSKU = req.params.productSKU;
    const product = _.findWhere(productsService.products, {numberSKU: productSKU});

    if(!product) {
        errorHandler.send(404, "Product not found!", req, res);
    } else {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<body>');
        res.write('<h1>' + productSKU +'</h1>');

        for(let i = 0; i < product.images.length; i++) {
            const image = __dirname + config.IMAGES_SKU_PRODUCTS + product.images[i]; // Imagem do código de barras do produto
            res.write('<img src="' + image + '" style="width:150px"><br>');
        }
        
        res.write('</body>');
        res.write('</html>');
        res.end();
    }
}

const dataRev = 0;

// Get all products - pagination
function getAllProducts(req, res, next) {
    // // 
    // var rev = req.header("If-Match");
    // if (rev != undefined && rev == dataRev) {
    //     //res.header("etag", data.rev);
    //     res.status(304).send("Not modified");
 
    //     console.log("message text requested. no response sent since client text is still valid");
    // }
    // else {
    //    const MAX_AGE = 10;
    //    //const now = new Date();
    //    //const expire = new Date(now.getSeconds()+MAX_AGE);
 
    //     res.set("ETag", dataRev);
    //     res.set("Cache-Control", "public, max-age="+MAX_AGE);
    //     //res.header("expires", convertDateToUTC(expire));
    //     res.json({ text: 'asd'});
 
    //     console.log("message text requested. text sent was:" + 'asd');
    // }


    //

    // To simulate the operation of the http cache, add a setTimeout() and verify that only on the first request will the response take time, on subsequent requests and until the cache expires, 
    // the response will be quickly sent
    // setTimeout(() => {
    //}, 5000) //setTimeout was used to simulate a slow processing request
    let pageNr = req.query.page;
    let size = req.query.size;

    if(!pageNr) {
        pageNr = 1;
    }
    if(!size) {
        size = 5;
    }
    
    // Verificar se a página introduzida é válida
    const totalPages = (Math.round(productsService.products.length/size));
    if(pageNr > totalPages) {
        errorHandler.send(404, 'Page not found', req, res);
        return;
    }

    const resultPaginated = utils.paginate(productsService.products, pageNr, size);
    let resultPaginatedFinal = null;
    let contentType = null;

    // Content negotiation
    res.format({ 
        html: function () { 
            contentType = 'text/html';
            let html_result = "<body><ul>";
            for(let i = 0; i < resultPaginated.length; i++) {
                const p = resultPaginated[i];
                html_result += "<li>SKU Number: " + p.numberSKU + "<br>Name: " + p.name + "<br>Description: " + p.description + "<br>Brand: " + p.brand + "<br>Unit price: " + p.unitPrice + "<br><br></li>";
            }
            html_result += "</ul></body>";
            resultPaginatedFinal = html_result;
        }, 
        text: function () { 
            contentType = 'text/plain';
            resultPaginatedFinal = '';
            for(let i = 0; i < resultPaginated.length; i++) {
                const p = resultPaginated[i];
                resultPaginatedFinal += "SKU Number: " + p.numberSKU + "\nName: " + p.name + "\nDescription: " + p.description + "\nBrand: " + p.brand + "\nUnit price: " + p.unitPrice + "\n\n";
            }
        },       
        json: function () { 
            contentType = 'application/json';
            resultPaginatedFinal = resultPaginated.map(({images, ...rest}) => ({_uri: "http://localhost:8002/products/" + rest.numberSKU, ...rest}));
        },
        'application/hal+json': function () { 
            contentType = 'application/hal+json';
            resultPaginatedFinal = resultPaginated.map(({images, ...rest}) => ({_uri: "http://localhost:8002/products/" + rest.numberSKU, ...rest}));
            resultPaginatedFinal = {
                data: resultPaginatedFinal,
                _links: resultPaginatedFinal.length > 0 ? createLinksCollection(pageNr, totalPages, size): ''
            }
        },
        'default': function() {
            errorHandler.send(406, 'Not Acceptable', req, res);
        }
    }); 
    errorHandler.send(200, resultPaginatedFinal, req, res, contentType, linkHeader(createLinksCollection(pageNr, totalPages, size)));
}

// get a product by name
function getProductByName(req, res, next) {
    const productName = req.params.productName;
    const product = _.findWhere(productsService.products, {name: productName});

    if(!product || product.length === 0) {
        errorHandler.send(404, "Product not found!", req, res);
    } else {
        let obj = {
            _uri: "http://localhost:8002/products/" + product.numberSKU,
            numberSKU: product.numberSKU,
            name: product.name,
            description: product.description,
            brand: product.brand,
            unitPrice: product.unitPrice
        }

        // avaliação agregada do produto: média ponderada de todas as classificações
        // Vai buscar todas as reviews deste produto
        let config = {
            headers: null,
            params: {
                product: product.numberSKU,
                status: 'approved',
                page: 1,
                size: 9999
            }
        }
    
        axios
            .get('http://localhost:8003/reviewsProcessed', config)
                .then(response => {
                    // Todas as reviews aprovadas deste produto
                    const reviewsOfProduct = response.data;
                    const numberOfReviews = response.data.length;
                    obj.ratingAverage = calculateAverageRating(numberOfReviews, reviewsOfProduct);

                    // Links
                    res.format({
                        'application/hal+json': function() {
                            const indexSelfItem = productsService.products.indexOf(product);
                            const isFirst = indexSelfItem === 0;
                            const isLast = (indexSelfItem + 1) === productsService.products.length;
                            
                            obj._links = createLinksItem(indexSelfItem, isFirst, isLast);
                            errorHandler.send(200, obj, req, res, 'application/hal+json', linkHeader(createLinksItem(indexSelfItem, isFirst, isLast)));
                        }
                    });
                })
                .catch(error =>
                    console.log(error)
                )
    }
}

// Get a product
function getProduct(req, res, next) {
    const productSKU = req.params.productSKU;
    const product = _.findWhere(productsService.products, {numberSKU: productSKU});

    if(!product || product.length === 0) {
        errorHandler.send(404, "Product not found!", req, res);
    } else {
        let obj = {
            _uri: "http://localhost:8002/products/" + product.numberSKU,
            numberSKU: product.numberSKU,
            name: product.name,
            description: product.description,
            brand: product.brand,
            unitPrice: product.unitPrice
        }

        // avaliação agregada do produto: média ponderada de todas as classificações
        // Vai buscar todas as reviews deste produto
        let config = {
            headers: null,
            params: {
                product: product.numberSKU,
                status: 'approved',
                page: 1,
                size: 9999
            }
        }
    
        axios
            .get('http://localhost:8003/reviews', config)
                .then(response => {
                    // Todas as reviews aprovadas deste produto
                    const reviewsOfProduct = response.data;
                    const numberOfReviews = response.data.length;
                    obj.ratingAverage = calculateAverageRating(numberOfReviews, reviewsOfProduct);

                    // Links
                    res.format({
                        'application/hal+json': function() {
                            const indexSelfItem = productsService.products.indexOf(product);
                            const isFirst = indexSelfItem === 0;
                            const isLast = (indexSelfItem + 1) === productsService.products.length;
                            
                            obj._links = {
                                self: {
                                    href: 'http://localhost:8002/products/' + product.numberSKU
                                },
                                collection: {
                                    href: 'http://localhost:8002/products'
                                },
                                'schema.org/image': {
                                    href: 'http://localhost:8002/products/' + product.numberSKU + '/images'
                                },
                                'psidi-reviews.org/reviews': {
                                    href: 'http://localhost:8002/products/' + product.numberSKU + '/reviews'
                                },
                                'psidi-reviews.org/publish-review': {
                                    href: 'http://localhost:8003/reviews'
                                }
                            }
                            errorHandler.send(200, obj, req, res, 'application/hal+json', linkHeader(createLinksItem(indexSelfItem, isFirst, isLast)));
                        }
                    });
                })
                .catch(error =>
                    console.log(error)
                )
    }
}

// Hypermedia controls
// Content-type apllication/hal+json
// Link Header
function linkHeaderElement(url, rel) {
    return "<" + url + ">;rel=" + rel;
}

function linkHeader(data) {
    var linkstring = "";
    var links = Object.keys(data).map(rel => linkHeaderElement(data[rel].href, rel));
    for (var i = 0; i <= links.length - 2; i++) {
        linkstring = linkstring.concat(links[i], ",");
    }
    linkstring = linkstring.concat(links[links.length - 1]);
    return linkstring
};

// Links Response 
function createLinksCollection(pageNr, totalPages, size) {
    pageNr = parseInt(pageNr);
    const firstPage = (pageNr === 1);
    const lastPage = (pageNr === totalPages);
    const existNextPage = (pageNr * size) < productsService.products.length;
    if(firstPage && !existNextPage) {
        return buildLink(['self', 'last'], [pageNr, totalPages, (pageNr + 1)], '/products?page=', '&size=' + size);
    }
    if(firstPage && existNextPage) {
        return buildLink(['self', 'last'], [pageNr, totalPages, (pageNr + 1)], '/products?page=', '&size=' + size);
    }
    if(lastPage) {
        return buildLink(['self', 'first', 'prev'], [pageNr, 1, (pageNr-1)], '/products?page=', '&size=' + size);
    }
    if(!firstPage && !lastPage) {
        return buildLink(['self', 'first', 'prev', 'last', 'next'], [pageNr, 1, (pageNr-1), totalPages, (pageNr + 1)], '/products?page=', '&size=' + size);
    }
}

function buildLink(relTypes, params, prefix, prefix2) {
    let links = {};
    for(let i = 0; i < relTypes.length; i++) {
        const relation = relTypes[i];
        links[relation] = {
            href: config.CATALOGUE_SERVICE_HOST + config.CATALOGUE_SERVICE_PORT + prefix + params[i] + prefix2
        }
    }
    return links;
}

function createLinksItem(indexSelfItem, isFirst, isLast) {
    const self = productsService.products[indexSelfItem].numberSKU;
    const next = (indexSelfItem + 1 <= (productsService.products.length - 1))? productsService.products[indexSelfItem + 1].numberSKU : null;
    const prev = (indexSelfItem - 1 >=0) ? productsService.products[indexSelfItem - 1].numberSKU : null;
    const last = productsService.products[productsService.products.length - 1].numberSKU;
    const first = productsService.products[0].numberSKU;
    if(isFirst) {
        return buildLink(['self', 'next', 'last'], [self, next, last], '/products', '');
    }
    if(isLast) { 
        return buildLink(['self', 'first', 'prev'], [self, first, prev], '/products', '');
    } 
    if(!isLast && !isFirst) {
        return buildLink(['self', 'first', 'next' , 'prev', 'last'], [self, first, next, prev, last], '/products/', '');
    }
}

// Helper functions
function calculateAverageRating(numberOfReviews, reviewsOfProduct) {
    let sum = 0;
    for(let j = 0; j < numberOfReviews; j++) {
        sum+= parseFloat(reviewsOfProduct[j].rating);
    }
    const average = sum/numberOfReviews;
    const averageRound = (Math.round(average * 10) / 10);
    return averageRound;
}

function tableFrequence(reviewsOfProduct, numberOfReviews) {
    let sum = 0;
    const tableFreq = [0, 0, 0, 0, 0];
    const obj = {};

    for(let j = 0; j < numberOfReviews; j++) {
        sum+= parseFloat(reviewsOfProduct[j].rating);

        if(reviewsOfProduct[j].rating >= 1 && reviewsOfProduct[j].rating < 2) { // 1 estrela
            tableFreq[0] +=1
        }
        if(reviewsOfProduct[j].rating >= 2 && reviewsOfProduct[j].rating < 3) { // 2 estrela
            tableFreq[1] +=1
        }
        if(reviewsOfProduct[j].rating >= 3 && reviewsOfProduct[j].rating < 4) { // 3 estrela
            tableFreq[2] +=1
        }
        if(reviewsOfProduct[j].rating >= 4 && reviewsOfProduct[j].rating < 5) { // 4 estrela
            tableFreq[3] +=1
        }
        if(reviewsOfProduct[j].rating >= 5) { // 5 estrela
            tableFreq[4] +=1
        }
    }
    const average = sum/numberOfReviews;
    const averageRound = (Math.round(average * 10) / 10);
    obj.ratingAverage = averageRound;

    // Tabela de frequências
    // Número de votos por estrela
                    
    // Número de review com 1 estrela
    obj.tableFreq = {
        "1": {
            "numberVotes": tableFreq[0],
            "percentage": tableFreq[0]/numberOfReviews * 100
        },
        "2": {
            "numberVotes": tableFreq[1],
            "percentage": tableFreq[1]/numberOfReviews * 100
        },
        "3": {
            "numberVotes": tableFreq[2],
            "percentage": tableFreq[2]/numberOfReviews * 100
        },
        "4": {
            "numberVotes": tableFreq[3],
            "percentage": tableFreq[3]/numberOfReviews * 100
        } ,
        "5": {
            "numberVotes": tableFreq[4],
            "percentage": tableFreq[4]/numberOfReviews * 100
        }
    }
    return obj;
}

function getReviewsByProduct(req, res, next) {
    // Shows all approved reviews (only published if approved)
    const productSKU = req.params.productSKU;
    let pageNr = req.query.page;
    let pageSize = req.query.size;

    if(!pageNr) {
        pageNr = 1;
    }
    if(!pageSize) {
        pageSize = 5;
    }

    // check if product exists
    const productExist = _.findWhere(productsService.products, {numberSKU: productSKU});
    if(productExist) {
        let config = {
            headers: null,
            params: {
                product: productSKU,
                status: 'approved',
                order: req.query.order,
                page: pageNr,
                size: pageSize, 
                auth: 0
            }
        }
    
        axios
            .get('http://localhost:8003/reviewsProcessed', config)
                .then(response => {
                    const finalObj = {
                        data: response.data,
                        _links: {
                            'psidi-reviews.org/products' : {
                                href: 'http://localhost:8002/products/' 
                            },
                            item: {
                                href: 'http://localhost:8002/products/' + productSKU
                            },
                            'psidi-reviews.org/reviews': {
                                href: 'http://localhost:8003/reviewsProcessed/:idReview'
                            }
                        }
                    }
                    res.json(finalObj);
                })
                .catch(error =>
                    console.log(error)
                )
    } else {
        errorHandler.send(404, "Product not found!", req, res);
    }
}

function getImagesByProduct(req, res, next) {
    
}

function getReviewsSummaryByProduct(req, res, next) {
    const productSKU = req.params.productSKU;
    const products = _.findWhere(productsService.products, {numberSKU: productSKU})
    if(!products || products.length === 0) {
        errorHandler.send(404, "Product not found!", req, res);
    } else {
        let obj = {
            _uri: "http://localhost:8002/products/" + products.numberSKU,
            numberSKU: products.numberSKU,
        }

        // avaliação agregada do produto: média ponderada de todas as classificações
        // Vai buscar todas as reviews deste produto
        let config = {
            headers: null,
            params: {
                product: products.numberSKU,
                status: 'approved',
                page: 1,
                size: 9999,
                auth: 0
            }
        }
    
        axios
            .get('http://localhost:8003/reviewsProcessed', config)
                .then(response => {
                    // Todas as reviews aprovadas deste produto
                    const reviewsOfProduct = response.data;
                    const numberOfReviews = response.data.length;
                    
                    const rating = tableFrequence(reviewsOfProduct, numberOfReviews)
                    obj = {...obj, rating};

                    errorHandler.send(200, obj, req, res, 'application/json');
                })
                .catch(error =>
                    console.log(error)
                )
    }
}

module.exports = router;