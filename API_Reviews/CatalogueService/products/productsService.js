///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// PRODUCTS SERVICE
///////////////////////////////////////////////////////

var products = [
    {   
        id: 1,
        numberSKU: "1234567891011", 
        name: "Product1", 
        description: "This is a product description 1",
        brand: "Brand Product 1", 
        unitPrice: "30.19", 
        images: ['1234567891011.png', 'image1_1234567891011.jpg'],
    },
    {
        id: 2,
        numberSKU: "1213141516171",
        name: "Product2",
        description: "This is a product description 2",
        brand: "Brand Product 2",
        unitPrice: "5.75",
        images: ['1213141516171.png', 'image1_1213141516171.png']
    },
    {
        id: 3,
        numberSKU: "1521564845151",
        name: "Product3",
        description: "This is a product description 2",
        brand: "Brand Product 3",
        unitPrice: "20.00",
        images: ['1521564845151.png', 'image1_1521564845151.png']
    }
];

async function addProduct(product) {
    const obj = {
        _uri: 'http://localhost:8002/products/' + product.numberSKU,
        name: product.name,
        description: product.description,
        brand: product.brand,
        unitPrice: product.unitPrice
    }
    products.push(product);
    return obj
}

module.exports = {
    products,
    addProduct
}