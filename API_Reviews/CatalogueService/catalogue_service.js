///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// CATALOGUE SERVICE
///////////////////////////////////////////////////////

"use strict";

// Imports section
require('rootpath')();
const express = require('express'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    config = require('config.json'),
    log4js = require("log4js"),
    logger = log4js.getLogger();

logger.level = "debug";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Routes
app.use('/products', require('./products/productsController'));

// Start service
const server = app.listen(config.CATALOGUE_SERVICE_PORT, function () {
    logger.info('Server listening on port ' + config.CATALOGUE_SERVICE_PORT);
});