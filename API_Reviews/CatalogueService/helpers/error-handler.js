///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// HELPER ERROR HANDLER
///////////////////////////////////////////////////////

function send(errorCode, errorMessage, req, res, contentType, links) {
    switch(errorCode) {
        case 400: 
            res.status(errorCode).json(errorMessage);
        break;
        case 401:
            break;
        case 404:
            res.status(errorCode).json(errorMessage);
        break;  
        case 406:
            res.status(errorCode).json(errorMessage);
        break;
        case 200:
            res.set('Link', links)
            if(contentType === 'application/json' || contentType === 'application/hal+json') {
                if(typeof (errorMessage) === 'string') {
                } else {
                    res.json(errorMessage);
                }
            }
            if(contentType === 'text/html') {
                res.status(errorCode).send('<html>' + errorMessage + '</html>');
            }
            if(contentType === 'text/plain') {
                res.status(errorCode).send(errorMessage);
            }
        break;
        case 201:
            res.status(errorCode).json(errorMessage);
        break;
    }
}

module.exports = {
    send
};