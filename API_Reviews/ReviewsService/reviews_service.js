///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS SERVICE
///////////////////////////////////////////////////////

"use strict";

// Imports section
const express = require('express'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    path = require('path'),
    config = require('./config.json'),
    log4js = require("log4js"),
    logger = log4js.getLogger();

require('rootpath')();
logger.level = "debug";

app.use(express.static(__dirname + '/docs')); // Documentation folder

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Reviews
app.use('/', require('./reviews/reviewsController'));

// Reviews Processed
app.use('/', require('./reviewsProcessed/reviewsProcessedController'));

// Votes
app.use('/', require('./votes/votesController'));

// Reports 
app.use('/', require('./reports/reportsController'));


// Billboard API
app.get('/', function (req, res) {
    const linksHeader = {
        'psidi-reviews.org/products': 'http://localhost:8002/products',
        'psidi-reviews.org/users': 'http://localhost:8001/users',
        'psidi-reviews.org/auth': 'http://localhost:8001/auth'
    }
    const _links = {
        'psidi-reviews.org/products': {
            href: 'http://localhost:8002/products'
        },
        'psidi-reviews.org/users': {
            href: 'http://localhost:8001/users'
        },
        'psidi-reviews.org/auth': {
            href: 'http://localhost:8001/auth'
        }
    }

    res.format({
        'application/hal+json': function() {
            res.links(linksHeader);
            res.json({_links});
        },
        'application/json': function() {
            res.links(linksHeader);
            res.json({_links});
        },
        'text/html': function () {
            res.links(linksHeader);
            const html = ("<html><body>" + 
                    "<li>Product Catalog: <a href='http://localhost:8002/products'>psidi-reviews.org/products</a></li>" + 
                    "<li>Add new user: <a href='http://localhost:8001/users'>psidi-reviews.org/users</a></li>" + 
                    "<li>Authenticates a user: <a href='http://localhost:8001/auth'>psidi-reviews.org/auth</a></li>"
                + "</body></html>");
            res.send(html)
        }
    });
})

// Documentation
app.get('/documentation', function (req, res) {
    res.sendFile(path.join(__dirname,'./docs/documentation/index.html')); 
})

app.get('/documentation/swagger-users-service', function (req, res) {    
    res.sendFile(path.join(__dirname, config.OPEN_API_JSON_FILE_USERS_SERVICE));
})

app.get('/documentation/swagger-catalogue-service', function (req, res) {    
    res.sendFile(path.join(__dirname, config.OPEN_API_JSON_FILE_CATALOGUE_SERVICE));
})

app.get('/documentation/swagger-reviews-service', function (req, res) {    
    res.sendFile(path.join(__dirname, config.OPEN_API_JSON_FILE_REVIEWS_SERVICE));
})

app.get('/swagger-ui', function (req, res) {    
    res.sendFile(path.join(__dirname,'./docs/swagger-ui/index.html')); 
})

// Start service
const server = app.listen(config.REVIEW_SERVICE_PORT, function () {
    logger.info('Server listening on port ' + config.REVIEW_SERVICE_PORT);
});