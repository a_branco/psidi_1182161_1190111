///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS CONTROLLER
///////////////////////////////////////////////////////

"use strict";

// Import section
const express = require('express'),
    router = express.Router(),
    votesService = require('./votesService'),
    reviewsProcessedService = require('../reviewsProcessed/reviewsProcessedService'),
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    errorHandler = require('../helpers/error-handler'),
    axios = require ('axios'),
    jwt_module = require('jsonwebtoken'),
    jwtconfig = require ('../jwt_config.json'),
    _ = require('underscore');

logger.level = "debug";


// ROUTES
// GET
// POST
router.post('/reviewsProcessed/:idReview/votes', voteReview) // Add a new review

function voteReview(req, res, next) {
    const idReview = req.params.idReview;
    console.log(idReview)

    // verificar se eziste
    const rev = reviewsProcessedService.reviewsProcessed;
    const ezit = _.findWhere(rev, {idReview: idReview});
    if(ezit) {
        //res.json(ezit)

        // username de quem esta a votar
        const actualAuth = req.headers.authorization;
        const tokenAuth =  actualAuth && actualAuth.split(' ')[1];
        const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
        const userLogged = jwtAuthInfo.username;
        console.log(userLogged)

        const vote = {
            idReview: idReview,
            username: userLogged
        }
        votesService.addVote(vote)
            .then(r => {
                errorHandler.send(200, r, req, res);
            })
            .catch(next)
    } else {
        console.log('review nao ezist')
    }

    // const idReview = req.params.idReview;

    // // check review ezist
    // const reviewEzist1 = _.findWhere(reviewsService.reviews, {idReview: idReview});
    // if(!reviewEzist1) {
    //     errorHandler.send(400, 'Review not found!', req, res);
    // } else {
    //     reviewsService.getReview(idReview)
    //         .then(r => {
    //             errorHandler.send(200, r, req, res);
    //         })
    //         .catch(next)
    // }
}


module.exports = router;