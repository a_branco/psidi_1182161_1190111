///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// VOTES SERVICE
///////////////////////////////////////////////////////

// Sections import
const _ = require('underscore');
const reviewsService = require('../reviews/reviewsService');

var votes = [];

function getVotesByReview(idReview) {
    return _.filter(votes, (n) => n.idReview === idReview);
}

async function addVote(vote) {
    const isDuplicate = _.filter(votes, (v) => v.idReview === vote.idReview && v.username === vote.username);
    if(!isDuplicate || votes.length === 0) { 
        const obj = {
            voteId: votes.length + 1,
            idReview: vote.idReview, 
            username: vote.username
        }
        votes.push(obj)
       
        const final = {
            _uri: 'http://localhost:8003/votes/' + obj.voteId,
            review: {
                _uri: 'http://localhost:8003/reviews/' + obj.idReview,
                idReview: obj.idReview        
            },
            username: obj.username
        }
        return final
    } else {
        console.log('nao pode votar duas vezes na mesma review')
    }
}

module.exports = {
    addVote,
    votes,
    getVotesByReview
};
