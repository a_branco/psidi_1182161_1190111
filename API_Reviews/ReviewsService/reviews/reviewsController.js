///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS CONTROLLER
///////////////////////////////////////////////////////

"use strict";

// Import section
const express = require('express'),
    router = express.Router(),
    reviewsService = require('./reviewsService'),
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    errorHandler = require('../helpers/error-handler'),
    axios = require ('axios'),
    jwt_module = require('jsonwebtoken'),
    jwtconfig = require ('../jwt_config.json'),
    _ = require('underscore');

logger.level = "debug";

// CONSTANTS
const filtersEnum = ['username', 'status', 'product'];

// ROUTES
// GET
router.get('/reviews/:idReview', getReview) // Retorna uma review pending
router.get('/reviews', getReviewsPending) // Retorna reviews pending

// POST
router.post('/reviews', addNewReview) // Add a new review
router.delete('/reviews/:idReview', deleteReview) // Delete a review

function deleteReview(req, res, next) {
    const idReview = req.params.idReview;
    const reviewExist = _.findWhere (reviewsService.reviews, {idReview: idReview});

    if(reviewExist) { // A review existe, verificar se o user logado é o dono da review
        const auth = req.headers.authorization;
        const username = getUsernameLogged(auth);

        if(reviewExist.username !== username){// sem permissoes para eliminar a review
            errorHandler.send(403, 'No permissions to delete this review.', req, res);
        } else {
            reviewsService.deleteReview(idReview, username)
                .then(r => {
                    console.log("ok")
                })
                .catch(next) 
        }
        
    } else{
        errorHandler.send(404, "not found", req, res)
    }
}

function getUsernameLogged(authorization) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.username;
}

function getReviewsPending(req, res, next) {
    const username = req.query.username;
    //const status = req.query.status;
    const product = req.query.product;
    const orderBy = req.query.order;
    const pageNr = req.query.page;
    const pageSize = req.query.size;

    const filtersReviews = [];
    if(username) {
        filtersReviews.push({
            type: filtersEnum[0],
            value: username
        })
    }
    // if(status) {
    //     filtersReviews.push({
    //         type: filtersEnum[1],
    //         value: status
    //     })
    // }
    if(product) {
        filtersReviews.push({
            type: filtersEnum[2],
            value: product
        })
    }

    reviewsService.getReviewsFilter(filtersReviews, orderBy, pageNr, pageSize)
        .then(r => {
            res.json(r)
        })
        .catch(next)
}

function getReview(req, res, next) {
    const idReview = req.params.idReview;

    // check review ezist
    const reviewEzist1 = _.findWhere(reviewsService.reviews, {idReview: idReview});
    if(!reviewEzist1) {
        errorHandler.send(400, 'Review not found!', req, res);
    } else {
        reviewsService.getReview(idReview)
            .then(r => {
                errorHandler.send(200, r, req, res);
            })
            .catch(next)
    }
}

function addNewReview(req, res, next) {
    const actualAuth = req.headers.authorization;
    let review = req.body;
    const tokenAuth =  actualAuth && actualAuth.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    const userLogged = jwtAuthInfo.username;
    review.username = userLogged;

    if(userLogged) { 
        reviewsService.addNewReview(review)
            .then(r => {
                logger.info("New review published!");
                errorHandler.send(202, 'The result of your review will be published at: ' + r, req, res);
                console.log("»»» Accepted POST to new resource " + r);
            })
            .catch(next)
    } else {
        logger.error("Forbidden");
        res.status(403).json("Forbbiden")
    }
}

module.exports = router;