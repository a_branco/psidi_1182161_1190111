///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS SERVICE
///////////////////////////////////////////////////////

// Sections import
const _ = require('underscore');
const utils = require('../helpers/utils');
const moment = require('moment');

var reviews = [
    { idReview:'1', text: "A little unhappy with the product.", rating: "2.5", product: "1234567891011", date: '01/12/2020', status: 'pending', username: "user"},
    { idReview:'2', text: "This is a text", rating: "4", product: "1234567891011", date: moment().format('DD/MM/YYYY'), status: 'pending',  username: "abranco"}
];

async function deleteReview(idReview) {
    const review = _.findWhere(reviews, {idReview: idReview});
    if(review.votes === 0) { // apenas se nao existirem votos é que pode ser eleiminada
        const index = reviews.indexOf(review); 
        if(index !== -1) {
            reviews.splice(index, 1);
        }
        return true;
    } else {
        return false;
    }
}
  
async function getReviewsFilter(filters, orderBy, page, size) {
    // Filtering
    if(!page) {
        page = 1;
    }
    if(!size) {
        size = 5;
    }

    let filterObj = {}
    for(let i = 0; i < filters.length; i++) {
        const filter = filters[i];
        if(filter.type === 'username') {
            filterObj = {...filterObj, 'username': filter.value}    
        }
        if(filter.type === 'status') {
            filterObj = {...filterObj, 'status': filter.value}    
        }
        if(filter.type === 'product') {
            filterObj = {...filterObj, 'product': filter.value}    
        }
    }
    
    let reviewsFinal = [];
    if(filterObj) {
        reviewsFinal = reviews.filter(function(item) {
            for (var key in filterObj) {
                if (item[key] === undefined || item[key] != filterObj[key])
                return false;
            }
            return true;
        });
    } else {
        reviewsFinal = reviews;
    }
    
    // Order by
    if(orderBy === 'desc') {
        reviewsFinal.sort(function(a, b){
            var aa = a.date.split('/').reverse().join(),
                bb = b.date.split('/').reverse().join();
            return aa < bb ? -1 : (aa > bb ? 1 : 0);
        }).reverse();
    }
    if(orderBy === 'asc') {
        reviewsFinal.sort(function(a, b){
            var aa = a.date.split('/').reverse().join(),
                bb = b.date.split('/').reverse().join();
            return aa < bb ? -1 : (aa > bb ? 1 : 0);
        });
    }
   
    const reviewsObj = reviewsFinal.map(({...rest}) => ({_uri: "http://localhost:8003/reviews/" + rest.idReview, ...rest}));

    // Pagination
    const reviewsPaginated = utils.paginate(reviewsObj, page, size);
    return reviewsPaginated;
}

async function getReview(idReview) {
    return reviews.filter(r => r.idReview === idReview)
}

async function addNewReview (review) {
    const newID = Math.random().toString(36).substr(2, 9);;
    
    // Queue the request - handle it when possible
    setTimeout(function(){
        console.log("Handling request " + newID);
        const reviewToAdd = {
            idReview: newID.toString(),
            text: review.text,
            rating: review.rating,
            product: review.product,
            username: review.username,
            date: moment().format('DD-MM-YYYY').toString()
        }
        reviews.push(reviewToAdd);			
    }, Math.random()*15000);
        
   return "http://localhost:8003/reviewsProcessed/" + newID;
}

module.exports = {
    addNewReview,
    getReview,
    getReviewsFilter,
    deleteReview,
    reviews
};