///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REPORTS SERVICE
///////////////////////////////////////////////////////

// Sections import
const _ = require('underscore');
const reviewsService = require('../reviewsProcessed/reviewsProcessedService');

var reports = [];

async function reportReview(report) {
    const obj = {
        reportId: report.length + 1,
        idReview: report.idReview, 
        username: report.username
    }
    reports.push(obj);

    const review = _.findWhere(reviewsService.reviewsProcessed, {idReview: report.idReview});
    review.status = 'reported'; // o status é alterado para reported e esta deve deizar de estar visivel

    const final = {
        _uri: 'http://localhost:8003/reports/' + obj.reportId,
        review: {
            _uri: 'http://localhost:8003/reviews/' + obj.idReview,
            idReview: obj.idReview        
        },
        username: obj.username
    }

    // depois de reportada uma review deve ser removida do público, até que um moderador aprove ou rejeite o report


    return final
}

module.exports = {
    reportReview
};
