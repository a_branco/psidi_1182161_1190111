///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS PROCESSED SERVICE
///////////////////////////////////////////////////////

// Sections import
const _ = require('underscore');
const reviewsService = require('../reviews/reviewsService');
const votesService = require('../votes/votesService');
const utils = require('../helpers/utils')

var reviewsProcessed = [
    { idReview:'3', text: "5 star product. very satisfied.", rating: "5", product: "1234567891011", approvalData: '20/05/2020', moderator: "moderator", status: 'approved', username: 'user'}
];


// async function getAllReviewsProcessed() {
//     return reviewsProcessed;
// }
async function getAllReviewsProcessed(filters, orderBy, page, size) {
    // Filtering
    if(!page) {
        page = 1;
    }
    if(!size) {
        size = 5;
    }

    let filterObj = {}
    for(let i = 0; i < filters.length; i++) {
        const filter = filters[i];
        if(filter.type === 'username') {
            filterObj = {...filterObj, 'username': filter.value}    
        }
        if(filter.type === 'status') {
            filterObj = {...filterObj, 'status': filter.value}    
        }
        if(filter.type === 'product') {
            filterObj = {...filterObj, 'product': filter.value}    
        }
    }
    
    let reviewsFinal = [];
    if(filterObj) {
        reviewsFinal = reviewsProcessed.filter(function(item) {
            for (var key in filterObj) {
                if (item[key] === undefined || item[key] != filterObj[key])
                return false;
            }
            return true;
        });
    } else {
        reviewsFinal = reviewsProcessed;
    }
    
    // Order by
    if(orderBy === 'desc') {
        reviewsFinal.sort(function(a, b){
            var aa = a.date.split('/').reverse().join(),
                bb = b.date.split('/').reverse().join();
            return aa < bb ? -1 : (aa > bb ? 1 : 0);
        }).reverse();
    }
    if(orderBy === 'asc') {
        reviewsFinal.sort(function(a, b){
            var aa = a.date.split('/').reverse().join(),
                bb = b.date.split('/').reverse().join();
            return aa < bb ? -1 : (aa > bb ? 1 : 0);
        });
    }
   
    // vai buscar os votos de cada uma das reviews
    for(let i = 0; i<reviewsFinal.length; i++) {
        const votes = votesService.getVotesByReview(reviewsFinal[i].idReview);
        if(votes) {
            reviewsFinal[i].votes =  votes.length;
        } else {
            reviewsFinal[i].votes =  0;
        }
        console.log(votes)
    }

    const reviewsObj = reviewsFinal.map(({...rest}) => ({_uri: "http://localhost:8003/reviewsProcessed/" + rest.idReview, ...rest}));

    // Pagination
    const reviewsPaginated = utils.paginate(reviewsObj, page, size);
    return reviewsPaginated;
}


async function getProcessedReview(idReview) {
    // A review pode estar ou nao ja processada
    // Verificar se está processada
    let review = null;
    let reviewProcessed =  _.findWhere(reviewsProcessed, {idReview: idReview});
    if(!reviewProcessed) { // ou ainda não foi processada ou nem sequer eziste
        // verificar se eziste mas está pendente
        const reviewPending = _.findWhere(reviewsService.reviews, {idReview: idReview});
        if(reviewPending) {  // review eziste mas esta pendente de processamento
            review = reviewPending;
            review.status = 'pending';
        }
    } else { // esta processada (ir buscar os votos da review)
        review = reviewProcessed;
        console.log('buscar votos da review')
        const votes = votesService.getVotesByReview(idReview);
        console.log(votes)
        if(votes) {
            review.votes = votes.length;
        } else {
            review.votes = 0;
        }
        
    }
    return review;
}


async function deleteReview(idReview) {
    const review = _.findWhere(reviewsProcessed, {idReview: idReview});
    console.log(review)
        const index = reviewsProcessed.indexOf(review); 
        if(index !== -1) {
            reviewsProcessed.splice(index, 1);
        }
        return true;
    
}

async function processReportedReview(review) {
    let a = _.findWhere(reviewsProcessed, {idReview: review.idReview});
    a.status = review.status;
    console.log(a)
}

async function processReview(review) {
    const pendingReview = _.findWhere(reviewsService.reviews, {idReview: review.idReview});
    review.text = pendingReview.text;
    review.rating = pendingReview.rating;
    review.product = pendingReview.product;
    review.username = pendingReview.username;
    review.publishData = pendingReview.date;
    reviewsProcessed.push(review);
    return review;
}

module.exports = {
    processReview,
    getProcessedReview,
    processReportedReview,
    getAllReviewsProcessed,
    reviewsProcessed,
    deleteReview
};