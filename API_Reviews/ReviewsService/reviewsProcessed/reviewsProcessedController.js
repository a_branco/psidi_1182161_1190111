///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// REVIEWS PROCESSED CONTROLLER
///////////////////////////////////////////////////////

"use strict";

// Import section
const express = require('express'),
    router = express.Router(),
    reviewsProcessedService = require('./reviewsProcessedService'),
    votesService = require('../votes/votesService'),
    reviewsService = require('../reviews/reviewsService'),
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    errorHandler = require('../helpers/error-handler'),
    axios = require ('axios'),
    jwt_module = require('jsonwebtoken'),
    jwtconfig = require ('../jwt_config.json'),
    _ = require('underscore'),
    moment = require('moment');
logger.level = "debug";


// CONSTANTS
const filtersEnum = ['username', 'status', 'product'];

// PUT
router.put('/reviewsProcessed/:idReview', processedReview) // Add a new review

// POST


// GET
router.get('/reviewsProcessed/:idReview', getProcessedReview) // Retorna uma review já processada
router.get('/reviewsProcessed', getAllReviewsProcessed)
router.delete('/reviewsProcessed/:idReview', deleteReview) // Delete a review

function deleteReview(req, res, next) {
    const idReview = req.params.idReview;
    const reviewExist = _.findWhere (reviewsProcessedService.reviewsProcessed, {idReview: idReview.toString()});
    if(reviewExist) { // A review existe, verificar se o user logado é o dono da review
        const auth = req.headers.authorization;
        const username = getUsernameLogged(auth);

        if(reviewExist.username !== username){// sem permissoes para eliminar a review
            errorHandler.send(403, 'No permissions to delete this review.', req, res);
        } else {
            // apenas pode remover um review se esta não tiver votos
            const votes = votesService.getVotesByReview(idReview);
            if(votes.length === 0 || !votes) {
                reviewsProcessedService.deleteReview(idReview)
                .then(r => {
                    console.log("ok")
                })
                .catch(next) 
            } else {
                console.log('Review com votos nao pode ser apagada')
            }
            
        }
        
    } else{
        errorHandler.send(404, "not found", req, res)
    }
}

function getUsernameLogged(authorization) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.username;
}

function getReviews(mode, username, status, product, orderBy, pageNr, pageSize, res, next) {
    const filtersReviews = [];
    if(username) {
        filtersReviews.push({
            type: filtersEnum[0],
            value: username
        })
    }
    if(status) {
        filtersReviews.push({
            type: filtersEnum[1],
            value: status
        })
    }
    if(product) {
        filtersReviews.push({
            type: filtersEnum[2],
            value: product
        })
    }

    
    
    if(mode === 'all') { // pending 
        reviewsService.getReviewsFilter(filtersReviews, orderBy, pageNr, pageSize)
            .then(respon => {
                let allProcessedReviews = reviewsProcessedService.reviewsProcessed.map(({...rest}) => ({_uri: "http://localhost:8003/reviewsProcessed/" + rest.idReview, ...rest}));

                // reportadas nao devem estar visiveis
                allProcessedReviews = allProcessedReviews.filter(r => r.status !== 'reported')

                const fi = respon.concat(allProcessedReviews);
                res.json(fi)  
            })
            .catch(next)
    }
    if(mode === 'processed') { // só processadas
        reviewsProcessedService.getAllReviewsProcessed(filtersReviews, orderBy, pageNr, pageSize)
            .then(r => {
                 // reportadas nao devem estar visiveis a menos que o filtro seja status = reported
                 if(status !== 'reported' || !status) {
                    let aa = r.filter(r => r.status !== 'reported')

                    res.json(aa)  
                 } else {
                    res.json(r)
                 }
            })
            .catch(next)
    }
}

function getAllReviewsProcessed(req, res, next) {
    const username = req.query.username;
    const status = req.query.status;
    const product = req.query.product;
    const orderBy = req.query.order;
    const pageNr = req.query.page;
    const pageSize = req.query.size;

    if(req.query.auth === '0') { // significa que é um cliente anonimo que esta a ver as reviews de um produto e por isso não é preciso autenticar
        getReviews('processed', username, status, product, orderBy, pageNr, pageSize, res, next);
    } else if (req.query.auth !== '-1' && req.query.auth !== '0') {
        // Apenas moderador
        const actualAuth = req.headers.authorization;
        const tokenAuth =  actualAuth && actualAuth.split(' ')[1];
        const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
        const userLogged = jwtAuthInfo.username;
        const isModerator = (jwtAuthInfo.role === 'moderator');

        if(isModerator || userLogged === username) { // aqui interessa tambem ver as reviews pending
            getReviews('processed', username, status, product, orderBy, pageNr, pageSize, res, next);
        } else {
            errorHandler.send(403, 'No permissions to view all reviews.', req, res);
        }    
    } else {
        getReviews('all', username, status, product, orderBy, pageNr, pageSize, res, next);
    }
}

let dataRev = 0;

function getProcessedReview(req, res, next) {
    const idReview = req.params.idReview;

    // var rev = req.header("If-Match");
    // if (rev != undefined && rev == dataRev) {
    //     //res.header("etag", data.rev);
    //     res.status(304).send("Not modified");
 
    //     console.log("message text requested. no response sent since client text is still valid");
    // }
    // else {
    //    const MAX_AGE = 10;
    //    //const now = new Date();
    //    //const expire = new Date(now.getSeconds()+MAX_AGE);
 
    //     res.set("ETag", dataRev);
    //     res.set("Cache-Control", "public, max-age="+MAX_AGE);
    //     //res.header("expires", convertDateToUTC(expire));
    //     res.json({ text: 'asd'});
 
    //     console.log("message text requested. text sent was:" + 'asd');
    // }



    reviewsProcessedService.getProcessedReview(idReview)
        .then(r => {
            if(r) {
                let objFinal = {
                    _uri: 'http://localhost:8003/reviewsProcessed/' + r.idReview,
                    text: r.text,
                    rating: r.rating,
                    product: r.product,
                    approvalData: r.approvalData,
                    moderator: r.moderator,
                    status: r.status,
                    username: r.username,
                    votes: 0
                }
                            
                objFinal._links = {
                    'self': {
                        href: 'http://localhost:8003/reviewsProcessed/' + r.idReview
                    },
                    'author': {
                        href: 'http://localhost:8001/users/' + objFinal.username
                    },
                    'psidi-reviews.org/products': {
                        href: 'http://localhost:8002/products/' + objFinal.product
                    },
                    'psidi-reviews.org/votes': {
                        href: 'http://localhost:8003/reviewsProcessed/' + r.idReview + '/votes'
                    },
                    'psidi-reviews.org/reports': {
                        href: 'http://localhost:8003/reviewsProcessed/' + r.idReview + '/reports'
                    }
                }
                res.json(objFinal)    
            } else {
                res.status(404).send('not found')
            }
        })
        .catch(next)
}

function processedReview(req, res, next) {
//     console.log("PUT body: " + JSON.stringify(req.body));

//   var message = req.body; 

//   var rev = req.header("If-Match");

//   if (rev === undefined) {
//     res.set("ETag", dataRev).status(400).send("Missing header 'if-match'");
//     console.log("message text was NOT updated since the request had no etag");   
//   }
//   else if (rev == dataRev) {
//     data.messageText = message.text;
//     data.original = false;
//     dataRev++;

//     res.set("ETag", dataRev);
//     res.json({ text: data.messageText, changed: !data.original });

//     console.log("message text was updated to:" + data.messageText);   
//   }
//   else {
//     res.set("ETag", dataRev).status(412).send("Precondition failed");
//     console.log("message text was NOT updated since request was out of date (received revision " + rev + " but server revision is " + dataRev);   
//   }

    const actualAuth = req.headers.authorization;
    const tokenAuth =  actualAuth && actualAuth.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    const userLogged = jwtAuthInfo.username;
    const isModerator = (jwtAuthInfo.role === 'moderator');

    if(isModerator) { // Apenas moderador pode aprovar review
        const idReview = req.params.idReview;
        const status = req.body.status;
        const moderador = userLogged; 
        const review = {
            idReview: idReview,
            moderator: moderador,
            status: status,
            approvalData: moment().format('DD-MM-YYYY').toString(),
        }
        
        // Aqui pode ser aprovar/rejeitar uma review pendente 
       

        let isReportedReview = false;
        const reviewReported = _.findWhere(reviewsProcessedService.reviewsProcessed, {idReview: idReview});
        if(reviewReported && reviewReported.status === 'reported') {
            isReportedReview = true;
        }
        if(isReportedReview) {
            reviewsProcessedService.processReportedReview(review)
                .then(r => {
                    // cenas
                    console.log('aprovado report')
                })
                .catch(next)
        } else {
            reviewsProcessedService.processReview(review)
                .then(r => {
                    logger.info("New review processed!");
                    console.log("Review processada com o status: " + review.status);

                    reviewsService.deleteReview(review.idReview); // A review foi processada, no entanto continua como pending no recurso de reviews
                })
                .catch(next)
        }
        // Ou pode se aprovar/rejeitar uma review reportada
    } else {
        errorHandler.send(403, 'No permissions to approve or reject reviews.', req, res);
    }
}

module.exports = router;