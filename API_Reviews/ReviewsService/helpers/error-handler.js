///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// HELPER ERROR HANDLER
///////////////////////////////////////////////////////

function send(errorCode, errorMessage, req, res) {
    switch(errorCode) {
        case 400: 
            res.status(errorCode).json(errorMessage);
        break;
        case 401:
            res.status(errorCode).json(errorMessage);
        break;
        case 404:
            res.status(errorCode).json(errorMessage);
        break;  
        case 200:
            if(typeof (errorMessage) === 'string') {
            } else {
                res.json(errorMessage);
            }
        break;
        case 202:
            res.status(errorCode).set('Location', errorMessage).json(errorMessage); 
        break;
        case 403: 
            res.status(errorCode).json(errorMessage);
        break;
    }
}

module.exports = {
    send
};