///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// HELPER UTILS
///////////////////////////////////////////////////////

function paginate(array, pageNr, size) {
    pageNr = Math.abs(parseInt(pageNr));
    pageNr = pageNr > 0 ? pageNr - 1 : pageNr;
    size = parseInt(size);
    size = size < 1 ? 1 : size;

    // Filter
    return a = [...(array.filter((value, n) => {
        return (n >= (pageNr * size)) && (n < ((pageNr+1) * size))
    }))]
}

module.exports = {
    paginate
}