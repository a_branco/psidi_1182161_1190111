///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// USERS SERVICE
///////////////////////////////////////////////////////

// Sections import
const _ = require('underscore');
const utils = require('../../CatalogueService/helpers/utils');
const config = require('../config.json');

var users = [
    { id: 1, username: 'user', password: Buffer.from('test').toString('base64'), firstname: 'Test', lastname: 'User', email: "email_teste@asd.com", role: 'user' },
    { id: 2, username: 'test', password: Buffer.from('test').toString('base64'), firstname: 'Test', lastname: 'Test', email: "adasd.asdasd@aasd.com", role: 'user' },
    { id: 3, username: 'abranco', password: Buffer.from('abranco').toString('base64'), firstname: 'Alexandra', lastname: 'Branco', email: "andlaksd@ioansd.com", role: 'user'},
    { id: 4, username: 'admin', password: Buffer.from('admin').toString('base64'), role: "admin"}
];

async function getUserInfo(username) { 
    const user = _.findWhere(users, {username: username});
    return {
        _uri: config.USERS_RESOURCE + user.username,
        username: user.username, 
        firstname: user.firstname, 
        lastname: user.lastname,
        email: user.email,
        role: user.role
    }
}

async function deleteUser(username) {
    const indexToDelete = users.indexOf(_.findWhere(users, {username: username}));
    users.splice(indexToDelete, 1);
    return indexToDelete 
}

async function getAllUsers (pageNr, pageSize) {
    // Removed administrators
    const allUsers = users.filter(function(u) {
        return u.role !== 'admin';
    });
    const usersWithoutPassword = allUsers.map(({id, password, ...rest}) => ({_uri: "http://localhost:8002/users/" + rest.username, ...rest}));
    const result = utils.paginate(usersWithoutPassword, pageNr, pageSize);
    return result;
}

async function addNewUser (user) {
    const newUser = {
        id: users.length + 1,
        username: user.username, 
        password: Buffer.from(user.password).toString('base64'),
        firstname: user.firstname, 
        lastname: user.lastname,
        email: user.email,
        role: "user"
    }
    users.push(newUser);
    return {
        _uri: config.USERS_RESOURCE + newUser.username,
        username: user.username, 
        firstname: user.firstname, 
        lastname: user.lastname,
        email: user.email,
        role: newUser.role,
        _links : {
            'psidi-reviews.org/auth': {
                href: 'http://localhost:8001/auth'
            }
        }
    }
}

async function updateUser(username, newPassword) {
    const userToUpdate = _.findWhere(users, {username: username});
    userToUpdate.password = newPassword;

    users.map(u => {
        if(u.username === username) {
            u.password = Buffer.from(newPassword).toString('base64');
        }
    })
    return userToUpdate;
}

async function getUser(username) {
    const user = _.findWhere(users, {username: username});
    if(user) {
        user.uri = "http://localhost:8002/users/" + user.username;
    }
    return user;
}


// Helper functions

module.exports = {
    users,
    getUser,
    getUserInfo,
    updateUser,
    addNewUser,
    deleteUser,
    getAllUsers
};