///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// USERS CONTROLLER
///////////////////////////////////////////////////////

"use strict";

const usersService = require('./usersService');

// Import section
const express = require('express'),
    router = express.Router(),
    userService = require('./usersService'),
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    errorHandler = require('../helpers/error-handler'),
    axios = require('axios'),
    _ = require('underscore'),
    jwtconfig = require ('../jwt_config.json'),
    jwt_module = require('jsonwebtoken');

logger.level = "debug";

// ROUTES
router.post('/', addNewUser) // Add new user to collection (without authentication)
router.get('/:username/reviews', getReviewsByUser); // Get reviews of user

router.get('/:username', getUser) // Get user

// Get a user (only administrators or user owner)
function getUser(req, res, next) {
    const username = req.params.username;
    const actualAuth = req.headers.authorization;

    const userOwner = checkAuth(actualAuth, username);
    const adminUser = checkIfAdministratorUser(actualAuth);

    const userExist = _.findWhere(usersService.users, {username: username});
    if(!userExist) {
        errorHandler.send(404, 'User not found', req, res);
        return;
    }

    if(userOwner || adminUser) { // Return user info
        // Check if user exist
        const userExist = _.findWhere(usersService.users, {username: username});
        if(userExist) {
            userService.getUserInfo(username)
                .then(userProfile => {
                    errorHandler.send(200, userProfile, req, res);
                })
                .catch(error =>
                    console.log(error)
                )
        } else { 
            errorHandler.send(404, 'User not found', req, res);
        }
    } else {
        errorHandler.send(403, 'No permissions to see this user', req, res);
    }
}

function getReviewsByUser(req, res, next){
    const username = req.params.username;
    const actualAuth = req.headers.authorization;
    const tokenAuth =  actualAuth && actualAuth.split(' ')[1];
    
    // Check if user exist
    const userExist = _.findWhere(userService.users, {username: username});
    if(userExist) { 
        // Pagination
        let pageNr = req.query.page;
        let pageSize = req.query.size;

        if(!pageNr) {
            pageNr = 1;
        }
        if(!pageSize) {
            pageSize = 5;
        }

        const filterStatus = req.query.status;

        let config = {
            headers: {'Authorization': 'Bearer ' + tokenAuth},
            params: {
                username: username,
                status: filterStatus,
                page: pageNr,
                size: pageSize,
                auth: -1
            }
        }

        const validAuth = getUsernameLogged(actualAuth, username) === username;
        if(validAuth) { 
            console.log('estou a ver as reviews do user logado') // retornar as aprovadas e as por aprovar tambem
            axios
                .get('http://localhost:8003/reviewsProcessed', config)
                    .then(response => {
                        console.log(response)
                        res.setHeader("Content-Type", "application/json");

                        const _links = {
                            'self': {
                                href: 'http://localhost:8003/users/' + username + '/reviews'
                            },
                            'author': {
                                href: 'http://localhost:8001/users/' + username
                            }
                        }
                        let objF = {data: response.data, 
                            _links
                        };
                        res.json(objF);

                    })
                    .catch(error =>
                        console.log(error)
                    )
        } else {
            errorHandler.send(403, 'Without permissions to complete action.', req, res);
        }
    } else {
        errorHandler.send(404, 'User not found!', req, res);
    }
}

// Helper functions
function getUsernameLogged(authorization, username, role) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.username;
}

function checkAuth(authorization, username) { 
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.username === username;
}

function checkIfAdministratorUser(authorization) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.role === 'admin';
}


function addNewUser(req, res, next) {
    const user = req.body;
    if(!user.username || !user.firstname || !user.lastname || !user.password || !user.email) { // Checks if all mandatory fields are sent correctly
        errorHandler.send(400, "Mandatory fields to fill.", req, res);
    } else {
        // Check if user exists
        let userExist = _.findWhere(userService.users, {username: user.username});
        if(userExist) { // Already exists 
            console.log("Username already exists.")
            errorHandler.send(409, "Username already exists.", req, res);
        } else {
            userService.addNewUser(user)
                .then(user => {
                    res.setHeader("Content-Type","application/hal+json");
                    const link = {
                        'psidi-reviews.org/auth': 'http://localhost:8001/auth'
                    }
                    res.links(link)
                    logger.info("New user added: " + user.username);
                    errorHandler.send(201, user, req, res);
                })
                .catch(next)
        }
    }
}

module.exports = router;