﻿///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// USERS SERVICE
///////////////////////////////////////////////////////

// NPM Dependencies
require('rootpath')();
const express = require('express'), 
    app = express(), 
    cors = require('cors'), 
    bodyParser = require('body-parser'), 
    jwt_module = require('jsonwebtoken'),
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    _ = require('underscore'),
    multipart = require('connect-multiparty');

// Imports section
const usersService = require('./users/usersService'),
    moderatorsService = require('./moderators/moderatorsService'),
    errorHandler = require('helpers/error-handler'),
    config = require ('config.json'),
    jwtconfig = require ('jwt_config.json'),
    jwt = require('helpers/jwt');

logger.level = "debug";

var multipartMiddleware = multipart();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Use JWT auth to secure the API
app.use(jwt());

// Handler error of JWT Authentication
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        let contentType = '', err = '';
        res.format({ 
            text: function () { 
                contentType = 'text/plain';
                err = 'Unauthorized. You must first authenticate in order to perform this request.'
            },       
            json: function () { 
                contentType = 'application/json'
                err = {
                    error: 'Unauthorized. You must first authenticate in order to perform this request.'
                }
            },
            'default': function() {
                errorHandler.send(406, 'Not Acceptable', req, res);
            }
        }) 
        logger.error("Invalid Credentials");
        res.setHeader("Content-Type", contentType);
        errorHandler.send(401, err, req, res, contentType);
    }
});

// ROUTES
app.use('/users', require('./users/usersController'));

// Admin
app.use('/api/admin', require('./admin/admin')); 

// Returns info of user logged
app.get('/auth/me', function (req, res){    
    const actualAuth = req.headers.authorization;
    const usernameLogged = getUsernameLogged(actualAuth);
    const users = usersService.users;

    if(usernameLogged) { 
        const user = _.findWhere(users, {username: usernameLogged})
        const obj = {
            _uri: config.USERS_RESOURCE + usernameLogged,
            username: user.username, 
            firstname: user.firstname, 
            lastname: user.lastname,
            email: user.email,
            role: user.role
        }
        errorHandler.send(200, obj, req, res);
    }
})

// Update a password of user
app.put('/auth/me/password', function (req, res) { 
    const actualAuth = req.headers.authorization;
    const usernameLogged = getUsernameLogged(actualAuth);
    const newUser = req.body;

    let contentType = null;
    if(usernameLogged) {
        // Content negotiation
        res.format({ 
            text: function () { 
                contentType = 'text/plain';
                if(!newUser.password || !newUser.confirmPassword) { 
                    errorHandler.send(400, 'Mandatory fields to be filled', req, res, contentType);
                } else {
                    if(newUser.password !== newUser.confirmPassword) { 
                        errorHandler.send(400, 'Passwords do not match', req, res, contentType);
                    }
                }
            },       
            json: function () { 
                contentType = 'application/json';
                if(!newUser.password || !newUser.confirmPassword) { 
                    const err1 = {
                        error: 'Mandatory fields to be filled'
                    }
                    errorHandler.send(400, err1, req, res, contentType);
                } else { 
                    if(newUser.password !== newUser.confirmPassword) { 
                        const err2 = {
                            error: 'Passwords do not match'
                        }
                        errorHandler.send(400, err2, req, res, contentType);
                    } else {
                        usersService.updateUser(usernameLogged, newUser.password)
                            .then(user => {
                                const obj = {
                                    _uri: config.USERS_SERVICE_HOST + 'users/' + user.username,
                                    username: user.username, 
                                    firstname: user.firstname, 
                                    lastname: user.lastname,
                                    email: user.email,
                                    role: user.role
                                }
                                errorHandler.send(200, obj, req, res, contentType);
                            })
                            .catch(err => {
                                console.log(err)
                            })
                    }
                }
            },
            'default': function() {
                errorHandler.send(406, 'Not Acceptable', req, res);
            }
        })    
    }
});

// Authenticates a user
app.post('/auth', multipartMiddleware, function (req, res) { 
    const username = req.body.username;
    const password = Buffer.from(req.body.password).toString('base64');
    const users = usersService.users;
    const moderators = moderatorsService.moderators;

    const user = users.filter(u => u.username === username && u.password === password);
    const moderator = moderators.filter(u => u.username === username && u.password === password);
   
    if(user[0]) { // Valid user
        const token = jwt_module.sign({ userId: user[0].id, username: user[0].username, role: user[0].role }, jwtconfig.secret, { expiresIn: '7d' }); // Generate token
        let contentType = null;

        // Content negotiation
        res.format({ 
            text: function () { 
                contentType = 'text/plain';
                errorHandler.send(200, token, req, res, contentType);
            }, 
            json: function () {
                contentType = 'application/hal+json';
                const obj = {
                    token: token,
                    _links: {
                        'psidi-reviews.org/auth/me': {
                            href: 'http://localhost:8001/auth/me'
                        },
                        'psidi-reviews.org/auth/me/password': {
                            href: 'http://localhost:8001/auth/me/password'
                        },
                        'psidi-reviews.org/reviews': {
                            href: 'http://localhost:8001/auth/me'
                        },
                    }
                }
              
                res.set('Link', buildHeaderLink(obj._links) + 'type=\'application/hal+json\'');
                res.set('Content-type', contentType)
                errorHandler.send(200, obj, req, res, contentType);
            },
            'default': function() {
                errorHandler.send(406, 'Not Acceptable', req, res);
            }
        }); 
        logger.info("User authenticated (" + username + ")");
    } else if(moderator[0]) { // Valid moderator
        const token = jwt_module.sign({ userId: moderator[0].id, username: moderator[0].username, role: moderator[0].role }, jwtconfig.secret, { expiresIn: '7d' }); // Generate token

        let contentType = null;

        // Content negotiation
        res.format({ 
            json: function () {
                contentType = 'application/hal+json';
                const obj = {
                    token: token,
                    _links: {
                        'psidi-reviews.org/auth/me': {
                            href: 'http://localhost:8001/auth/me'
                        },
                        'psidi-reviews.org/auth/me/password': {
                            href: 'http://localhost:8001/auth/me/password'
                        },
                        'psidi-reviews.org/products': {
                            href: 'http://localhost:8001/products'
                        },
                        'psidi-reviews.org/reviews': {
                            href: 'http://localhost:8001/reviews'
                        },
                        'psidi-reviews.org/users': {
                            href: 'http://localhost:8001/users'
                        },
                    }
                }
              
                res.set('Link', buildHeaderLink(obj._links) + 'type=\'application/hal+json\'');
                res.set('Content-type', contentType)
                errorHandler.send(200, obj, req, res, contentType);
            },
            'default': function() {
                errorHandler.send(406, 'Not Acceptable', req, res);
            }
        }); 

        logger.info("Moderator authenticated (" + username + ")");
    } else {
        // Content negotiation
        res.format({ 
            text: function () { 
                contentType = 'text/plain';
                errorHandler.send(401, "Invalid credentials", req, res, contentType);
            },       
            json: function () { 
                contentType = 'application/json';
                const obj = {
                    'error': "Invalid credentials"
                }
                errorHandler.send(401, obj, req, res, contentType);
            },
            'default': function() {
                errorHandler.send(406, 'Not Acceptable', req, res);
            }
        }); 
        logger.error("Invalid credentials (" + req.body.username + ")");
    }
})

// Helper functions
function getUsernameLogged(authorization) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.username;
}

function buildHeaderLink(obj) {
    let linkHeader = '';
    for (var link in obj) {
        linkHeader += '<' + obj[link].href + '>;rel=' + link + ';';
    }
    return linkHeader;
}

// Start service
const server = app.listen(config.USERS_SERVICE_PORT, function () {
    logger.info('Server listening on port ' + config.USERS_SERVICE_PORT);
});