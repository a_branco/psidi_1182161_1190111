///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// HELPER ERROR HANDLER
///////////////////////////////////////////////////////

function send(errorCode, errorMessage, req, res, contentType) {
    if(!contentType) { contentType = 'application/json'}
    switch(errorCode) {
        case 400: 
            res.status(errorCode).json(errorMessage);
        break;
        case 401:
            if(contentType === 'application/json') {
                res.status(errorCode).json(errorMessage);
            }
            if(contentType === 'text/plain') {
                res.status(errorCode).send(errorMessage);
            }
        break;
        case 404:
            res.status(errorCode).json(errorMessage);
        break;  
        case 200:
            if(contentType === 'application/hal+json') {
                res.json(errorMessage);
            }
            if(contentType === 'application/json') {
                if(typeof (errorMessage) === 'string') {
                    res.json(errorMessage);
                } else {
                    res.json(errorMessage);
                }
            }
            if(contentType === 'text/html') {
                res.status(errorCode).send('<html>' + errorMessage + '</html>');
            }
            if(contentType === 'text/plain') {
                res.status(errorCode).send(errorMessage);
            }
        break;
        case 403: 
            res.status(errorCode).json(errorMessage);
        break;
        case 201:
            res.status(errorCode).json(errorMessage);
        break;
        case 409:
            res.status(errorCode).json(errorMessage);
        break;
        case 406: 
            res.status(errorCode).json(errorMessage);
        break;
    }
}

module.exports = {
    send
};