const expressJwt = require('express-jwt');
const config = require('jwt_config.json');

function jwt() {
    const { secret } = config;
    return expressJwt({ secret, algorithms: ['HS256'] }).unless({
        path: [ // Public routes that don't require authentication
            '/auth',
            '/users',
            '/'
        ]
    });
}

module.exports = jwt;