///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// ADMIN ROUTES
///////////////////////////////////////////////////////

const moderatorsService = require('../moderators/moderatorsService');
const usersService = require('../users/usersService');

// Imports section
const express = require('express'),
    router = express.Router(),
    axios = require('axios'), 
    log4js = require("log4js"),
    logger = log4js.getLogger(),
    jwt_module = require('jsonwebtoken'),
    jwtconfig = require ('../jwt_config.json'),
    errorHandler = require('../helpers/error-handler'),
    _ = require('underscore');
logger.level = "debug";


// ROUTES
// POST
router.post('/products', addNewProduct);

// GET
router.get('/users', getAllUsers);
router.get('/moderators', getAllModerators); 

// DELETE
router.delete('/users/:username', deleteUser);

// FUNCTIONS
function getAllModerators (req, res, next) {
    const actualAuth = req.headers.authorization;
    const validAuth = checkIfAdministratorUser(actualAuth);
    
    let pageNr = req.query.page;
    let size = req.query.size;
    if(!pageNr) {
        pageNr = 1;
    }
    if(!size) {
        size = 5;
    }

    if(validAuth) {
        moderatorsService.getAllModerators(pageNr, size)
            .then(r => {
                errorHandler.send(200, r, req, res);
            })
            .catch(err => {
                console.log(err)
            })
    } else {
        errorHandler.send(403, "No permissions to view moderators list.", req, res)
        logger.error("No permissions to view moderators list.");
    }
}

function deleteUser(req, res, next) {
    const actualAuth = req.headers.authorization;
    const validAuth = checkIfAdministratorUser(actualAuth);
    const username = req.params.username;

    // Check if user exist
    const userExist = _.findWhere(usersService.users, {username: username});
    if(userExist) {
        if(validAuth) { 
            usersService.deleteUser(username)
                .then(r => {
                    console.log(r)
                    errorHandler.send(200, 'User unregistered with success!', res, res);   
                })
                .catch(next)
        } else {
            errorHandler.send(403, 'Without permissions to delete a user.', req, res);
        }
    } else {
        errorHandler.send(404, 'User not found!', req, res);
    }
}

// Get all users (only administrators)
function getAllUsers(req, res, next) {
    // Pagination
    let pageNr = req.query.page;
    let size = req.query.size;
    if(!pageNr) { 
        pageNr = 1;
    }
    if(!size) { 
        size = 5;
    }

    const actualAuth = req.headers.authorization;
    const validAuth = checkIfAdministratorUser(actualAuth, 'admin');
    if(validAuth) { 
        usersService.getAllUsers(pageNr, size)
            .then(users => {
                res.json(users)
            })
            .catch(next);
    } else { 
        errorHandler.send(403, 'No permissions to complete the action.', req, res);
    }
}

function addNewProduct(req, res, next) {
    const actualAuth = req.headers.authorization;
    const validAuth = checkIfAdministratorUser(actualAuth, 'admin');
    const tokenAuth =  actualAuth && actualAuth.split(' ')[1];

    if(validAuth) {
        let config = {
            headers: {'Authorization': 'Bearer ' + tokenAuth},
            params: req.body,
            data: req.body
        }
        axios
            .post('http://localhost:8002/products', config)
                .then(response => {
                    res.json(response.data);
                })
                .catch(error =>
                    console.log(error)
                )
    } else {
        errorHandler.send(403, "No permissions to add products.", req, res)
        logger.error("No permissions to add products.");
    }
}

// Helper functions
function checkIfAdministratorUser(authorization) {
    const tokenAuth =  authorization && authorization.split(' ')[1];
    const jwtAuthInfo = jwt_module.verify(tokenAuth, jwtconfig.secret);
    return jwtAuthInfo.role === 'admin';
}

module.exports = router;