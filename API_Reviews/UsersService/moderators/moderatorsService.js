///////////////////////////////////////////////////////
//
// Alexandra Branco, nº 1182161
// João Cardoso, nº 1190111
// PSIDI / MEI / ISEP
// (c) 2020
//
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// MODERATORS SERVICE
///////////////////////////////////////////////////////

const utils = require('../helpers/utils')
var moderators = [
    { id: 1, username: 'moderator', password: Buffer.from('test').toString('base64'), firstname: 'Test', lastname: 'User', email: "email_teste@asd.com", role: 'moderator' },
    { id: 2, username: 'test', password: Buffer.from('test').toString('base64'), firstname: 'Test', lastname: 'Test', email: "adasd.asdasd@aasd.com", role: 'moderator' },
    { id: 3, username: '1182161', password: Buffer.from('1182161').toString('base64'), firstname: 'Alexandra', lastname: 'Branco', email: "andlaksd@ioansd.com", role: 'moderator'}
];

async function getAllModerators (pageNr, pageSize) {
    const moderatorsWithoutPassword = moderators.map(({id, password, ...rest}) => ({_uri: "http://localhost:8001/moderators/" + rest.username, ...rest}));
    const result = utils.paginate(moderatorsWithoutPassword, pageNr, pageSize);
    return result;
}

module.exports = {
    moderators,
    getAllModerators
}